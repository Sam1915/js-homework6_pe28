// Exercise 1
// const product = {
//      name: 'Car',
//      price: 50000,
//      dicount: 3000,
//      sale: function() {
//           let dicountPrice = this.price - this.dicount;
//           return dicountPrice;
//      },
// }
// console.log(product.sale());


// Exercise 2
// function greetUser() {
//     let name = prompt("Введіть своє ім'я:");
//     let age = parseInt(prompt("Введіть свій вік:"));
 
//     const user = { 
//         name: name, 
//         age: age,
//     };
 
//     return `Привіт,мене звати ${user.name}, та мені ${user.age} років.`;
// }
 
// console.log(greetUser());


// Exercise 3
function cloneFunc(obj) {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }
    
    let clone = Array.isArray(obj) ? [] : {};
    
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            clone[key] = cloneFunc(obj[key]);
        }
    }
    
    return clone;
}
const origObj = {
    name: 'Alex',
    age: 25,
    city: 'Lviv',
    ['phone number']: 890-890-890,
}
const clonedObj = cloneFunc(origObj);

clonedObj.name = 'Sam';
clonedObj.age = 23;
clonedObj.city = 'Kiyv';
clonedObj['phone number'] = 725-725-725;

console.log(origObj);
console.log(clonedObj);